<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot . '/user/editlib.php');

class auth_international_form extends moodleform {
    function definition() {
        global $USER, $CFG, $DB;

        $mform = $this->_form;
        //$ip = $this->_customdata['ip'];
        $exp = $this->_customdata['exp'];
        $countries = array_merge(array('' => get_string('selectacountry')), $this->_customdata['countries']);
        $countryISOCode = $this->_customdata['countryISOCode'];
        $city = $this->_customdata['city'];
        $region = $this->_customdata['region'];
        //$regionName = $this->_customdata['regionName'];
        $zipCode = $this->_customdata['zipCode'];

        $usa_states = $this->_customdata['usa_states'];
        $canada_provinces = $this->_customdata['canada_provinces'];
        $states = auth_plugin_international::$COUNTRY_CODE_USA == $countryISOCode ? $usa_states : $canada_provinces;

        $mform->addElement('text', 'firstname', get_string('firstname', 'auth_international'), 'maxlength="100" size="25"');
        $mform->setType('firstname', PARAM_RAW_TRIMMED);
        $mform->addRule('firstname', get_string('missingfirstname'), 'required', null, 'server');

        $mform->addElement('text', 'lastname', get_string('lastname', 'auth_international'), 'maxlength="100" size="25"');
        $mform->setType('lastname', PARAM_RAW_TRIMMED);
        $mform->addRule('lastname', get_string('missinglastname'), 'required', null, 'server');

        $mform->addElement('text', 'email', get_string('email'), 'maxlength="100" size="25"');
        $mform->setType('email', PARAM_RAW_TRIMMED);
        $mform->addRule('email', get_string('missingemail'), 'required', null, 'server');

        $mform->addElement('text', 'username', get_string('account_name', 'auth_international'), 'maxlength="100" size="12"');
        $mform->setType('username', PARAM_NOTAGS);
        $mform->addRule('username', get_string('missing_account_name', 'auth_international'), 'required', null, 'server');

        if($exp == auth_plugin_international::$EXPERIENCE_SAME) {

            $mform->addElement('text', 'profile_field_accountnumber', get_string('accountnumber', 'auth_international'), 'maxlength="120" size="20"', 'required');
            $mform->addRule('profile_field_accountnumber', null, 'required', null, 'client');
            $mform->setType('profile_field_accountnumber', PARAM_TEXT);

            $mform->addElement('text', 'city', get_string('city'), 'maxlength="120" size="20"', 'required');
            $mform->addRule('city', null, 'required', null, 'client');
            $mform->setType('city', PARAM_TEXT);
            $mform->setDefault('city', $city);

            $mform->addElement('select', 'profile_field_state', get_string('state', 'auth_international'), $states, '', 'required');
            $mform->addRule('profile_field_state', null, 'required', null, 'client');
            $mform->setDefault('profile_field_state', $region);

            $mform->addElement('text', 'profile_field_zipcode', get_string('zipcode', 'auth_international'), 'maxlength="120" size="20"', 'required');
            $mform->addRule('profile_field_zipcode', null, 'required', null, 'client');
            $mform->setType('profile_field_zipcode', PARAM_TEXT);
            $mform->setDefault('profile_field_zipcode', $zipCode);

            $mform->addElement('select', 'country', get_string('country'), $countries);
            $mform->addRule('country', null, 'required', null, 'client');
            $mform->setDefault('country', $countryISOCode);

            if($field = $DB->get_record('user_info_field', array('shortname' => 'businessconsultant'))) {
            //if($field = $DB->get_record('user_info_field', array('shortname' => 'busconsultant'))) {
                require_once($CFG->dirroot.'/user/profile/field/menu/field.class.php');

                $formfield = new profile_field_menu($field->id, 0);
                $formfield->edit_field($mform);
            }
        } else {
            $mform->addElement('text', 'city', get_string('city'), 'maxlength="120" size="20"');
            $mform->addRule('city', null, 'required', null, 'client');
            $mform->setType('city', PARAM_TEXT);

            $mform->addElement('select', 'country', get_string('country'), $countries);
            $mform->addRule('country', null, 'required', null, 'client');
            $mform->setDefault('country', '');
        }

        // buttons
        $this->add_action_buttons(true, get_string('createaccount'));
    }

    function definition_after_data(){
        $mform = $this->_form;
        $mform->applyFilter('username', 'trim');

        // Next the customisable profile fields.
        profile_definition_after_data($mform, 0);
    }

    function validation($data, $files) {
        global $CFG, $DB;
        $errors = parent::validation($data, $files);

        $authplugin = get_auth_plugin($CFG->registerauth);

        if ($DB->record_exists('user', array('username'=>$data['username'], 'mnethostid'=>$CFG->mnet_localhost_id))) {
            $errors['username'] = get_string('usernameexists');
        } else {
            //check allowed characters
            if ($data['username'] !== core_text::strtolower($data['username'])) {
                $errors['username'] = get_string('usernamelowercase');
            } else {
                if ($data['username'] !== clean_param($data['username'], PARAM_USERNAME)) {
                    $errors['username'] = get_string('invalidusername');
                }
            }
        }

        //check if user exists in external db
        //TODO: maybe we should check all enabled plugins instead
        if ($authplugin->user_exists($data['username'])) {
            $errors['username'] = get_string('usernameexists');
        }

        if (! validate_email($data['email'])) {
            $errors['email'] = get_string('invalidemail');

        } else if ($DB->record_exists('user', array('email'=>$data['email']))) {
            $errors['email'] = get_string('emailexists').' <a href="forgot_password.php">'.get_string('newpassword').'?</a>';
        }

        if (!isset($errors['email'])) {
            if ($err = email_is_not_allowed($data['email'])) {
                $errors['email'] = $err;
            }

        }

        // Validate customisable profile fields. (profile_validation expects an object as the parameter with userid set)
        $dataobject = (object)$data;
        $dataobject->id = 0;
        $errors += profile_validation($dataobject, $files);

        return $errors;

    }
}
