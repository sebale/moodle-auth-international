<?php

$string['pluginname'] = 'International Registration';
$string['auth_pluginname'] = 'International Registration';
$string['auth_internationaldescription'] = 'International Registration Plugin';

$string['auth_emaildescription'] = '<p>International self-registration enables a user to create their own account via a \'Create new account\' button on the login page. The user then receives an email containing a secure link to a page where they can confirm their account. Future logins just check the username and password against the stored values in the Moodle database.</p><p>Note: In addition to enabling the plugin, email-based self-registration must also be selected from the self registration drop-down menu on the \'Manage authentication\' page.</p>';
$string['auth_emailnoemail'] = 'Tried to send you an email but failed!';
$string['auth_emailrecaptcha'] = 'Adds a visual/audio confirmation form element to the sign-up page for email self-registering users. This protects your site against spammers and contributes to a worthwhile cause. See http://www.google.com/recaptcha for more details.';
$string['auth_emailrecaptcha_key'] = 'Enable reCAPTCHA element';
$string['auth_emailsettings'] = 'Settings';

$string['account_name'] = 'Account Name';
$string['missing_account_name'] = 'Missing Account Name';


$string['accountnumber'] = 'Account Number';
$string['state'] = 'State';
$string['zipcode'] = 'Zip Code';
$string['businessconsultant'] = 'Business Consultant';

$string['firstname'] = 'First Name';
$string['lastname'] = 'Last Name';

$string['emailconfirmationsubject'] = '{$a}: account confirmation';
$string['emailconfirmation'] = 'Hi {$a->firstname},

A new account has been requested at \'{$a->sitename}\'
using your Account Name \'{$a->username}\' and temp Password \'{$a->password}\'


To confirm your new account, please go to this web address:

{$a->link}

In most mail programs, this should appear as a blue link
which you can just click on.  If that doesn\'t work,
then cut and paste the address into the address
line at the top of your web browser window.

If you need help, please contact the site administrator,
{$a->admin}';